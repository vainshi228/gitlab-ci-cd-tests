import { sum } from './main'

describe('main tests', () => {

  it.each([
    { creds: [1, 2] as [number, number], ans: 3 },
    { creds: [4, 4] as [number, number], ans: 8 },
    { creds: [3, 2] as [number, number], ans: 5 },
  ])('Sum testing', ({creds, ans}: {creds: [number, number], ans: number}) => {
    expect(sum(...creds)).toBe(ans);
  })
})